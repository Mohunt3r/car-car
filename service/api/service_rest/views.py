from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import AutomobileVO, Technician, Appointment
from .encoders import AutomobileVOEncoder, TechnicianEncoder, AppointmentEncoder

@require_http_methods(['GET', "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians":technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"error": "Invalid technician creation, try again"},
                status = 400
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid technician entered, try again"},
                status=404,
            )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=pk)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid technician id, try again"},
                status=404,
            )
        try:
            Technician.objects.filter(id=pk).update(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"error": "Invalid technician input, try again"},
                status=400,
            )

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            employee_number = content["technician"]
            technician = Technician.objects.get(employee_number=employee_number)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "This employee does not exist, try again"},
                status=422,
            )
        try:
            vin = content["vin"]
            vin = AutomobileVO.objects.get(vin=vin)
            content["vip_status"] = True
        except AutomobileVO.DoesNotExist:
            content["vip_status"] = False
        try:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"error": "Invalid appointment input, try again"},
                status=400,
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "This appointment does not seem to exist, try again"},
                status=404,
            )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            appointment = Appointment.objects.get(id=pk)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "This is not a pre-existing appointment, try again"},
                status=404,
            )
        if "technician" in content:
            try:
                employee_number = content["technician"]
                technician = Technician.objects.get(employee_number=employee_number)
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse(
                    {"error": "This employee does not exist, try again"},
                    status=422,
                )
        try:
            Appointment.objects.filter(id=pk).update(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"error": "Invalid appointment input, try again"},
                status=400,
            )
