from django.db import models
from django.urls import reverse
# Create your models here.


from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.vin


class SalesRep(models.Model):
    employee_name = models.CharField(max_length=50)
    employee_id = models.PositiveIntegerField(blank=False, unique=True)

    def __str__(self):
        return self.employee_name


class Customer(models.Model):
    customer_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=11, unique=True)

    def __str__(self):
        return self.customer_name


class AutoSale(models.Model):
    price = models.CharField(max_length=100)
    vehicle = models.ForeignKey(
        AutomobileVO,
        related_name="autosales",
        on_delete=models.PROTECT
    )
    salesrep = models.ForeignKey(
        SalesRep,
        related_name="autosales",
        on_delete=models.PROTECT,
        null=True
    )
    customer = models.ForeignKey(
        Customer,
        related_name="autosales",
        on_delete=models.PROTECT
    )
