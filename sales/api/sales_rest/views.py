from django.db import IntegrityError
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutoSale, AutomobileVO, Customer, SalesRep
from .encoders import (
    AutomobileVODetailEncoder,
    SalesRepListEncoder,
    SalesRepDetailEncoder,
    CustomerListEncoder,
    CustomerDetailEncoder,
    AutoSaleDetailEncoder,
    AutoSaleListEncoder,
)


@require_http_methods(["GET","POST"])
def api_salesreps(request):
    if request.method == "GET":
        salesreps = SalesRep.objects.all()
        return JsonResponse(
            {"salesreps": salesreps},
            encoder=SalesRepListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesrep = SalesRep.objects.create(**content)
        return JsonResponse(
            salesrep,
            encoder=SalesRepDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesrep(request, pk):
    if request.method == "GET":
        try:
            salesrep = SalesRep.objects.get(id=pk)
            return JsonResponse(
                salesrep,
                encoder=SalesRepDetailEncoder,
                safe=False
            )
        except AutoSale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesrep = SalesRep.objects.get(id=pk)
            salesrep.delete()
            return JsonResponse(
                salesrep,
                encoder=SalesRepDetailEncoder,
                safe=False,
            )
        except SalesRep.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            salesrep = SalesRep.objects.get(id=pk)

            props = []
            for prop in props:
                if prop in content:
                    setattr(salesrep, prop, content[prop])
            salesrep.save()
            return JsonResponse(
                salesrep,
                encoder=SalesRepDetailEncoder,
                safe=False,
            )
        except SalesRep.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse({
            'customer': customer},
            encoder=CustomerDetailEncoder,
            safe=False,)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)

            props = []
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        autosales = AutoSale.objects.all()
        return JsonResponse(
            {"autosales": autosales},
            encoder=AutoSaleListEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.get(customer_name=content["customer"])
        salesrep = SalesRep.objects.get(employee_name=content["salesrep"])
        automobile = content["vehicle"]
        automobile = AutomobileVO.objects.get(vin=automobile)
        content["vehicle"] = automobile
        content["salesrep"] = salesrep
        content["customer"] = customer
    autosale = AutoSale.objects.create(**content)
    return JsonResponse(
        autosale,
        encoder=AutoSaleDetailEncoder,
        safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = AutoSale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=AutoSaleDetailEncoder,
                safe=False
            )
        except AutoSale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = AutoSale.objects.get(id=pk)
            AutoSale.delete()
            return JsonResponse(
                sale,
                encoder=AutoSaleDetailEncoder,
                safe=False,
            )
        except AutoSale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            sale = AutoSale.objects.get(id=pk)

            props = []
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=AutoSaleDetailEncoder,
                safe=False,
            )
        except AutoSale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
