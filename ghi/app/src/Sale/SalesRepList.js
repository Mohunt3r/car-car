import React, { useState, useEffect } from 'react'

export default function SalesRepList() {
  const [salesreps, setSalesRep] = useState([])

  useEffect(() => {
    const getSalesRepData = async () => {
      const autoResponse = await fetch("http://localhost:8090/api/salesreps/");
      const autoData = await autoResponse.json();
      setSalesRep(autoData.salesreps);
    };

    getSalesRepData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">SalesReps</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Employee ID</th>
            </tr>
          </thead>
          <tbody>
            {salesreps.map((salesrep) => {
              return (
                <tr key={salesrep.id}>
                  <td>{salesrep.employee_name}</td>
                  <td>{salesrep.employee_id}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
