import React, { useEffect, useState } from 'react';

function ListAutoSales() {

  let [autosales, setAutosales] = useState([]);
  useEffect(() => {
      fetch("http://localhost:8090/api/autosales/")
          .then((response) => response.json())
          .then(res => setAutosales(res.autosales));
  }, []);

  return (
      <table className="table">
          <thead>
              <tr>
                  <th>Employee Name</th>
                  <th>Employee ID</th>
                  <th>Customer Name</th>
                  <th>VIN</th>
                  <th>Price</th>
              </tr>
          </thead>
          <tbody>
              {autosales?.map(autosale => {
                  return (
                      <tr key={autosale.id}>
                          <td>{autosale.salesrep.employee_name}</td>
                          <td>{autosale.salesrep.employee_id}</td>
                          <td>{autosale.customer.customer_name}</td>
                          <td>{autosale.vehicle.vin}</td>
                          <td>{autosale.price}</td>
                      </tr>
                  );
              })}
          </tbody>
      </table>
  );
}

export default ListAutoSales;
