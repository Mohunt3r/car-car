import React, { useState, useEffect } from 'react'

export default function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([])

  useEffect(() => {
    const getManufacturerData = async () => {
      const autoResponse = await fetch("http://localhost:8100/api/manufacturers/");
      const autoData = await autoResponse.json();
      setManufacturers(autoData.manufacturers);
    };

    getManufacturerData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map((manufacturer) => {
              return (
                <tr key={manufacturer.id}>
                  <td>{manufacturer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
