import React, { useState, useEffect } from 'react'

export default function VehicleModelForm() {
  const [name, setName] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const [manufacturer_id, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    const getManufacturerData = async () => {
      const manufacturerResponse = await fetch("http://localhost:8100/api/manufacturers/");
      const manufacturerData = await manufacturerResponse.json();
      setManufacturers(manufacturerData.manufacturers);
    };

    getManufacturerData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name,
      picture_url,
      manufacturer_id
    };
    const modelsUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(modelsUrl, fetchConfig);

    if (response.ok) {
      setName("");
      setPictureUrl("");
      setManufacturer("");
    }
  };

  const nameChange = (event) => {
    setName(event.target.value);
  };

  const pictureUrlChange = (event) => {
    setPictureUrl(event.target.value);
  };

  const manufacturerChange = (event) => {
    setManufacturer(event.target.value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new vehicle model</h1>
          <form
            onSubmit={handleSubmit}
            id="create-model-form"
          >
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={nameChange}
                value={name}
                placeholder="Model name"
                required
                type="text"
                name="name"
                id="name"
              />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={pictureUrlChange}
                value={picture_url}
                placeholder="Picture URL"
                required
                type="text"
                name="picture_url"
                id="picture_url"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                onChange={manufacturerChange}
                value={manufacturer_id}
                required
                id="manufacturer"
                name="manufacturer"
                className="form-select"
              >
                <option value="">Choose a manufacturer</option>
                {manufacturers.map((manufacturer) => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
