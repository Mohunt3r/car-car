import React, { useState, useEffect } from 'react'

export default function VehicleModelsList() {
  const [models, setModels] = useState([])

  useEffect(() => {
    const getModelData = async () => {
      const autoResponse = await fetch("http://localhost:8100/api/models/");
      const autoData = await autoResponse.json();
      setModels(autoData.models);
    };

    getModelData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Automobiles</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Manufacturer</th>
              <th scope="col">Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map((model) => {
              return (
                <tr key={model.id}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td>
                    <img src={model.picture_url} alt="car" height="100" />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
