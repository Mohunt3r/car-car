import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './Inventory/ManufacturersList';
import VehicleModelsList from './Inventory/VehicleModelsList';
import AutomobilesList from './Inventory/AutomobilesList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import VehicleModelForm from './Inventory/VehicleModelForm';
import AutomobileForm from './Inventory/AutomobileForm';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentsList from './Service/AppointmentsList';
import AppointmentHistory from './Service/AppointmentHistory';
import TechnicianForm from './Service/TechnicianForm';
import TechniciansList from './Service/TechniciansList';
import SalesForm from'./Sale/SalesForm';
import ListAutoSales from './Sale/ListAutoSales';
import CreateSalesRep  from './Sale/CreateSalesRep';
import CreateCustomer from './Sale/CreateCustomer';
import SalesRepHistory from './Sale/SalesRepHistory';
import SalesRepList from './Sale/SalesRepList';
export default function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route path="/models">
            <Route path="" element={<VehicleModelsList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="/manufacturers">
            <Route path="" element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<AutomobilesList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="/appointments">
            <Route path="" element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="/technicians">
            <Route path="" element={<TechniciansList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="/record" element={<SalesForm />} />
          <Route path="/saleshistory" element={<ListAutoSales />} />
          <Route path="/salespersons/new" element={<CreateSalesRep />} />
          <Route path="/customer" element={<CreateCustomer />} />
          <Route path="/saleslist" element={<SalesRepHistory />} />
          <Route path="/salespersons" element={<SalesRepList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
