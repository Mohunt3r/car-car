import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/"><b>Car Car</b></NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav">


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <b>Inventory</b>
              </p>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturers List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models">Vehicle Models List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles">Automobiles List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new">New Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/new">New Vehicle Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/new">New Automobile</NavLink></li>
              </ul>
            </li>


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <b>Sales</b>
              </p>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/saleslist">SalesPerson Sale History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/saleshistory">Sales Page</NavLink></li>
                <li><NavLink className="dropdown-item" to="/record">Record a Sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customer">Create a Customer</NavLink></li>
              </ul>
            </li>


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <b>Service</b>
              </p>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/appointments">Service Appointments List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new">New Service Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Service Appointment History</NavLink></li>
              </ul>
            </li>


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <b>Employees</b>
              </p>
              <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/salespersons">Sales People List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians">Technicians List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespersons/new">New Sales Person</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new">New Technician</NavLink></li>
              </ul>
            </li>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
