import React, { useState, useEffect } from 'react'

export default function AppointmentForm() {
  const [vin, setVin] = useState("");
  const [customer_name,setCustomerName] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [reason,setReason] = useState("");
  const [technician, setTechnician] = useState("");
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const getTechnicianData = async () => {
      const technicianResponse = await fetch("http://localhost:8080/api/technicians/");
      const technicianData = await technicianResponse.json();
      setTechnicians(technicianData.technicians);
    };

    getTechnicianData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      vin,
      customer_name,
      date,
      time,
      reason,
      technician,
    };

    const appointmentsUrl = "http://localhost:8080/api/appointments/";
    console.log(data);
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    console.log(data);
    const response = await fetch(appointmentsUrl, fetchConfig);

    if (response.ok) {
      setVin("");
      setCustomerName("");
      setDate("");
      setTime("");
      setReason("");
      setTechnician("");
    }
  };


  const vinChange = (event) => {
    setVin(event.target.value);
  };

  const customerNameChange = (event) => {
    setCustomerName(event.target.value);
  };

  const dateChange = (event) => {
    setDate(event.target.value);
  };

  const timeChange = (event) => {
    setTime(event.target.value);
  };

  const reasonChange = (event) => {
    setReason(event.target.value);
  };

  const technicianChange = (event) => {
    setTechnician(event.target.value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Service Appointment</h1>
          <form
            onSubmit={handleSubmit}
            id="create-appointment-form"
          >
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={vinChange}
                value={vin}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={customerNameChange}
                value={customer_name}
                placeholder="Customer Name"
                required
                type="text"
                name="customer_name"
                id="customer_name"
              />
              <label htmlFor="date">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={dateChange}
                value={date}
                placeholder="Date"
                required
                type="date"
                name="date"
                id="date"
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={timeChange}
                value={time}
                placeholder="Time"
                required
                type="time"
                name="time"
                id="time"
              />
              <label htmlFor="time">Time</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={reasonChange}
                value={reason}
                placeholder="reason"
                required
                type="text"
                name="reason"
                id="reason"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="mb-3">
              <select
                onChange={technicianChange}
                value={technician}
                required
                id="technician"
                name="technician"
                className="form-select"
              >
                <option value="">Choose a Technician</option>
                {technicians?.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.employee_number}>
                      {technician.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
