import React, { useState, useEffect } from 'react';

export default function AppointmentHistory() {
    const [list, setList] = useState();

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setList(data.appointments)
        }
    }

    const [query, setQuery] = useState('')

    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div>
            <br/>
            <input type="text" placeholder="Search by VIN" className="search" onChange={(event)=>setQuery(event.target.value)} />
            <br/>
            <br/>
            <h1>Service History</h1>
            <table className="table table-striped">
                <thead>
                    <tr className="table-success">
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>VIP Status</th>
                        <th>Reason</th>
                        <th>Technician</th>
                    </tr>
                </thead>
                <tbody>
                    {list?.filter((appointment) =>
                    appointment.vin.includes(query)
                    ).map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.vip_status.toString()}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.technician.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
