import React, { useState, useEffect } from 'react'

export default function TechniciansList() {
  const [technicians, setTechnicians] = useState([])

  useEffect(() => {
    const getTechnicianData = async () => {
      const autoResponse = await fetch("http://localhost:8080/api/technicians/");
      const autoData = await autoResponse.json();
      setTechnicians(autoData.technicians);
    };

    getTechnicianData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Technicians</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Employee Number</th>
            </tr>
          </thead>
          <tbody>
            {technicians.map((technician) => {
              return (
                <tr key={technician.id}>
                  <td>{technician.name}</td>
                  <td>{technician.employee_number}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
